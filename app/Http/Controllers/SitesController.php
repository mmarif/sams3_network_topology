<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Site;
use DB;

class SitesController extends Controller
{
    function show(Request $request)
	{
		$sites = Site::query();
		//$sites  = Site::query()->take(2000);
		$sites->whereNotNull('GPS_LAT');
		$sites->whereNotNull('GPS_LONG');
		
		if($request->has('SITE_STATE') && !empty($request->get('SITE_STATE'))) {
            $sites->where('SITE_STATE', $request->get('SITE_STATE'));
        }
		
		if($request->has('MINISTRY') && !empty($request->get('MINISTRY'))) {
            $sites->where('MINISTRY', $request->get('MINISTRY'));
        }
		
		if($request->has('AGENCY') && !empty($request->get('AGENCY'))) {
            $sites->where('AGENCY', $request->get('AGENCY'));
        }
		
		if($request->has('SITE_NAME') && !empty($request->get('SITE_NAME'))) {
            $sites->where('SITE_NAME', 'like', '%' . $request->get('SITE_NAME') . '%');
        }
		
		if($request->has('NET_HEALTH') && !empty($request->get('NET_HEALTH'))) {
            $sites->where('NET_HEALTH', $request->get('NET_HEALTH'));
        }
		
		return response()->json($sites->get());
	}
	
	function stats(Request $request)
	{
		//select NET_HEALTH, count(NET_HEALTH) as total from sites GROUP BY NET_HEALTH;
		$qry = "NET_HEALTH, count(NET_HEALTH) as total";

        $stats = Site::select(DB::raw($qry))
                    ->groupBy('NET_HEALTH');
					
        $stats->whereNotNull('GPS_LAT');
		$stats->whereNotNull('GPS_LONG');           
					
		if($request->has('SITE_STATE') && !empty($request->get('SITE_STATE'))) {
            $stats->where('SITE_STATE', $request->get('SITE_STATE'));
        }
		
		if($request->has('MINISTRY') && !empty($request->get('MINISTRY'))) {
            $stats->where('MINISTRY', $request->get('MINISTRY'));
        }
		
		if($request->has('AGENCY') && !empty($request->get('AGENCY'))) {
            $stats->where('AGENCY', $request->get('AGENCY'));
        }
		
		if($request->has('SITE_NAME') && !empty($request->get('SITE_NAME'))) {
            $stats->where('SITE_NAME', 'like', '%' . $request->get('SITE_NAME') . '%');
        }
		
		if($request->has('NET_HEALTH') && !empty($request->get('NET_HEALTH'))) {
            $stats->where('NET_HEALTH', $request->get('NET_HEALTH'));
        }

		return response()->json($stats->get());
	}
	
	function ministries()
	{
		$qry = "MINISTRY";
		$ministries = Site::select(DB::raw($qry))
                    ->groupBy('MINISTRY');
					
		
		return response()->json($ministries->get());
		
	}
	
	function agencies(Request $request)
	{
		$qry = "AGENCY";
		$agencies = Site::select(DB::raw($qry))
                    ->groupBy('AGENCY');
		
		if($request->has('MINISTRY') && !empty($request->get('MINISTRY'))) {
            $agencies->where('MINISTRY', $request->get('MINISTRY'));
        }
		
		return response()->json($agencies->get());
		
	}
}
