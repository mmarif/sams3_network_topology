<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
	protected $table = 'sams3_network_site_1govnet';
    protected $fillable = [
        'SITE_ID', 
		'SITE_NAME', 
		'MINISTRY', 
		'MINISTRY_AGENCY', 
		'AGENCY', 
		'SPECTRUM_SPEED',
		'SPECTRUM_SLG',
		'BMT_URL', 
		'NET_UTIL_IN', 
		'NET_UTIL_OUT', 
		'NET_HEALTH',
		'GPS_LAT', 
		'GPS_LONG', 
		'SITE_STATE',
		'SITE_REGION'
    ];
}
