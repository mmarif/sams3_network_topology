<!DOCTYPE html>
<html  lang="en" ng-app="samsApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Map showing the spread of government managed internet services sites" />
		<meta name="keywords" content="1GOVNET network sites" />

        <title>SAMS 3.0 | Network Sites</title>

		<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Ubuntu">
        <link type="text/css" rel="stylesheet" href="{{URL::asset('assets/bootstrap/dist/css/bootstrap.min.css')}}" >
		<link type="text/css" rel="stylesheet" href="{{URL::asset('assets/leaflet/leaflet.css')}}" />
		 <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="assets/leaflet/leaflet.ie.css" /><![endif]-->
		<link type="text/css" rel="stylesheet" href="{{URL::asset('assets/leaflet/plugins/leaflet.markercluster/MarkerCluster.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{URL::asset('assets/leaflet/plugins/leaflet.markercluster/MarkerCluster.Default.css')}}" />
		<link type="text/css" rel="stylesheet"  href="{{URL::asset('css/style.css')}}">

        <script src="{{URL::asset('assets/angular/angular.min.js')}}"></script>
        <script src="{{URL::asset('assets/lodash/dist/lodash.min.js')}}"></script>
        <script src="{{URL::asset('assets/angular-route/angular-route.min.js')}}"></script>
        <script src="{{URL::asset('assets/angular-local-storage/dist/angular-local-storage.min.js')}}"></script>
        <script src="{{URL::asset('assets/restangular/dist/restangular.min.js')}}"></script>


		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript">
		WebFontConfig = {
		google: {
		families: ['Ubuntu::latin']
		}
		};
		(function () {
		var wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
		})();
		</script>
		<![endif]-->
    </head>
	<body ng-controller="IndexController">
		<nav class="navbar navbar-fixed-top navbar-light bg-faded" style="background-color: #07192b;">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">1GOVNET TOPOLOGY</a>
				</div>
				<!-- Navbar content -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

					<form class="navbar-form navbar-right" role="form">
						<div class="form-group">
							<!-- <label for="filter">Site State</label> -->
							<select class="form-control" ng-model="filters.SITE_STATE">
								<option value="0" selected>Select State</option>
								<option value="PUJ">WP PUTRAJAYA</option>
								<option value="KL">WP KUALA LUMPUR</option>
								<option value="LBN">WP LABUAN</option>
								<option value="JHR">JOHOR</option>
								<option value="KDH">KEDAH</option>
								<option value="KEL">KELANTAN</option>
								<option value="MLK">MELAKA</option>
								<option value="NS">NEGERI SEMBILAN</option>
								<option value="PHG">PAHANG</option>
								<option value="PRK">PERAK</option>
								<option value="PLS">PERLIS</option>
								<option value="PPG">PULAU PINANG</option>
								<option value="SBH">SABAH</option>
								<option value="SWK">SARAWAK</option>
								<option value="SGR">SELANGOR</option>
							</select>
						</div>
						<!--<div class="form-group">
						 <label for="filter">Site Region</label> 
							<select class="form-control" ng-model="filters.SITE_STATE">
								<option value="0" selected>Select Region</option>
								<option value="1">Region 1</option>
									<option value="2">Region 2</option>
									<option value="3">Region 3</option>
									<option value="4">Region 4</option>
							</select>
						</div>-->
			  
						<div class="form-group">
							<!-- <label for="filter">Site Status</label> -->
							<select class="form-control" ng-model="filters.NET_HEALTH">
								<option value="0" selected>Select Status</option>
								<option value="UP">Up</option>
								<option value="DOWN">Down</option>
								<option value="PMW">Maintenance</option>
							</select>
						</div>
						<!--<div class="form-group">
							<label>Ministry Name</label> 
							<input type="text" class="form-control" placeholder="Ministry Name"  ng-model="filters.MINISTRY">
						</div>-->
						<div class="form-group">
							<!-- <label for="filter">Site Status</label> -->
							<select class="form-control" ng-model="filters.MINISTRY" ng-change="filterAgencies()">
								<option value="0" selected>Select Ministry</option>
								<option ng-selected="<% ministry.MINISTRY == filters.MINISTRY %>" ng-repeat="ministry in ministries" 
									value="<% ministry.MINISTRY %>"><% ministry.MINISTRY %>     
								</option>
							</select>
						</div>
						<div class="form-group">
							<!-- <label for="filter">Site Status</label> -->
							<select class="form-control" ng-model="filters.AGENCY" style="width:250px">
								<option value="0" selected>Select Agency</option>
								<option ng-selected="<% agency.AGENCY == filters.AGENCY %>" ng-repeat="agency in agencies" 
									value="<% agency.AGENCY %>"><% agency.AGENCY %>     
								</option>
							</select>
						</div>
						<div class="form-group">
							<!-- <label>Agency Name</label> -->
							<input type="text" class="form-control" placeholder="Search"  ng-model="filters.SITE_NAME">
						</div>
						<button type="button" ng-click="filter()" class="btn btn-default navbar-btn">
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
						</button>&nbsp; 
						<button type="button" ng-click="reset()" class="btn btn-default navbar-btn">
							<span class="">Reset</span>
						</button> 
					</form>   
				</div>
			</div>
		</nav>
		<div class="panel-group floating" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						NETWORK HEALTH
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body">
						<p class="totalvalue">Total Sites <% total_sites %></p>
						<div class="widget-row">
							<div class="widget-cell"><span class="icon-up icon-widget"></span>UP</div>
							<div class="widget-cell"><% sites_alive %></div>
						</div>
						<div class="widget-row">
							<div class="widget-cell"><span class="icon-down icon-widget"></span>DOWN</div>
							<div class="widget-cell"><% sites_down %></div>
						</div>
						<div class="widget-row">
							<div class="widget-cell"><span class="icon-pmw icon-widget"></span>PMW</div>
							<div class="widget-cell"><% sites_maintenance %></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="map"></div>
		<div id="loading-mask" class="modal-backdrop" style="display:none;"></div>
		<div id="loading" style="display:none;">
			<div class="loading-indicator">
				<img src="{{URL::asset('img/ajax-loader.gif')}}">
			</div>
		</div>
	
	
	
		<script type="text/javascript" src="{{URL::asset('assets/jquery/dist/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('assets/leaflet/leaflet.js')}}"></script>
		<script type="text/javascript" src="{{URL::asset('assets/leaflet/plugins/leaflet.markercluster/leaflet.markercluster.js')}}"></script>
		<script src="{{URL::asset('js/sams.js')}}"></script>
        <script src="{{URL::asset('js/controllers.js')}}"></script>
    </body>
</html>