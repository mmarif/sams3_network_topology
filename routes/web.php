<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/sites', 'SitesController@show');
Route::post('/sites', 'SitesController@show');

Route::get('/stats', 'SitesController@stats');
Route::post('/stats', 'SitesController@stats');

Route::get('/ministries', 'SitesController@ministries');
Route::post('/ministries', 'SitesController@ministries');

Route::get('/agencies', 'SitesController@agencies');
Route::post('/agencies', 'SitesController@agencies');