var samsAppControllers = angular.module('samsAppControllers', []);

samsAppControllers.controller('IndexController', ['$scope', '$http', '$interval', function ($scope, $http, $interval) {
	//console.log("IndexController");
    //var map, users, mapquest, firstLoad;	
	var vm = this;
	//vm.svrpath = "";
	//vm.svrpath = "http://localhost:8000";
	vm.svrpath = "http://samsv3.gitn.com.my/sams3/public";
	vm.refreshFrequency = "300000"; //60 seconds
	vm.firstLoad = true;
	
	vm.sites = null;
	vm.marker = null;
	$scope.sites_alive = 0;
	$scope.sites_down = 0;
	$scope.sites_maintenance = 0;
	$scope.total_sites = 0;
	$scope.filters = {
		'NET_HEALTH' : '0',
		'SITE_STATE' : '0',
		'MINISTRY' : '0',
		'AGENCY' : '0',
		'SITE_NAME' : ''
	};
	
	$scope.ministries = null;

	vm.users = new L.MarkerClusterGroup({
        spiderfyOnMaxZoom: true, 
        showCoverageOnHover: true, 
        zoomToBoundsOnClick: true,
        removeOutsideVisibleBounds: true
      });
	  
	vm.mapquest = new L.TileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
		maxZoom: 18,
        attribution: '&copy; <a href="http://developer.smartmapsapi.com/">TM Smartmap</a> | GITN Sdn Bhd'
      
      });
	vm.map = new L.Map('map', {
		center: new L.LatLng(2.829071252, 101.6028966),
        zoom: 2,
        layers: [vm.mapquest, vm.users]
	});

	// GeoLocation Control
	vm.geoLocate = function () {
		vm.map.locate({setView: true, maxZoom: 1});
	}
	
	vm.geolocControl = new L.control({
			position: 'topright'
		});
	vm.geolocControl.onAdd = function (map) {
		var div = L.DomUtil.create('div', 'leaflet-control-zoom leaflet-control');
		div.innerHTML = '<a class="leaflet-control-geoloc" href="#" ng-click="geoLocate(); return false;" title="My location"></a>';
		return div;
	};
      
	vm.map.addControl(vm.geolocControl);
	vm.map.addControl(new L.Control.Scale());
	
	vm.showMinistries = function() {
		$http.post('http://samsv3.gitn.com.my/sams3/public/ministries', $scope.filters, {})
			.then(
                    function(response){
                        // success callback
						$scope.ministries = response.data;
                    },
                    function(response){
                        // failure callback
                        console.log('Error in showMinistries:' + response.statusText);
                    }
            );
	};
	
	$scope.filterAgencies = function(){
		vm.showAgencies();
	};
	vm.showAgencies = function() {
		$http.post('http://samsv3.gitn.com.my/sams3/public/agencies', $scope.filters, {})
			.then(
                    function(response){
                        // success callback
						$scope.agencies = response.data;
                    },
                    function(response){
                        // failure callback
                        console.log('Error in showStats:' + response.statusText);
                    }
            );
	};
	
	vm.showSites = function() {
		$http.post('http://samsv3.gitn.com.my/sams3/public/sites', $scope.filters, {})
			.then(
                    function(response){
                        // success callback
                        vm.sites = response.data;
						vm.users.clearLayers();						
						vm.processSites();
                        console.log(vm.sites);
                    },
                    function(response){
                        // failure callback
                        console.log('Error in showSites:' + response.statusText);
                    }
            )
			.finally(function(){
				if (vm.firstLoad == true) {
					vm.map.fitBounds(vm.users.getBounds());
					vm.firstLoad = false;
				};
				
			});
	};
	
	vm.showStats = function() {
		$http.post('http://samsv3.gitn.com.my/sams3/public/stats', $scope.filters, {})
			.then(
                    function(response){
                        // success callback
						$scope.sites_alive = 0;
						$scope.sites_down = 0;
						$scope.sites_maintenance = 0;
						$scope.total_sites = 0;
						for(var i = 0; i < response.data.length; i++)
						{
							if(response.data[i].NET_HEALTH == "UP")
								$scope.sites_alive = response.data[i].total;
							else if(response.data[i].NET_HEALTH == "DOWN")
								$scope.sites_down = response.data[i].total;
							else if(response.data[i].NET_HEALTH == "PMW")
								$scope.sites_maintenance = response.data[i].total;
							$scope.total_sites = $scope.sites_alive + $scope.sites_down + $scope.sites_maintenance;
							
						}

                    },
                    function(response){
                        // failure callback
                        console.log('Error in showStats:' + response.statusText);
                    }
            );
	};
	
	vm.processSites = function() {
		for (var i = 0; i < vm.sites.length; i++) {
            var location = new L.LatLng(vm.sites[i].GPS_LAT, vm.sites[i].GPS_LONG);

            if(vm.sites[i].NET_HEALTH == "UP"){
              var blueDot = L.icon({
                iconUrl: 'img/green-map-marker.png'
              });
            } else if(vm.sites[i].NET_HEALTH == "DOWN") {
              var blueDot = L.icon({
                iconUrl: 'img/red-map-marker.png'
              });
            } else {
              var blueDot = L.icon({
                iconUrl: 'img/blue-map-marker.png'
              });
            }
          
			var ministry_agency = "<div style='font-size: 14px; color: #0078A8;'>" + vm.sites[i].MINISTRY_AGENCY + "</div>";

			var sitename = "<div style='font-size: 18px;'><b>"+ vm.sites[i].SITE_NAME +"</b></div>";

			var site_id = "<div style='font-size: 12px;text-align:left;'>Site ID: <b>"+ vm.sites[i].SITE_ID +"</b></div>";

			var net_speed = "<div style='font-size: 12px;text-align:left;'>Network Speed: <b>"+ vm.sites[i].SPECTRUM_SPEED +"</b></div>";

			var net_health = "<div style='font-size: 12px;text-align:left;'>Network Health: <b>"+ vm.sites[i].NET_HEALTH +"</b></div>";

			var net_utilin = "<div style='font-size: 12px;text-align:left;'>Utilization In: <b>"+ vm.sites[i].NET_UTIL_IN +"%</b></div>";

			var net_utilout = "<div style='font-size: 12px;text-align:left;'>Utilization Out: <b>"+ vm.sites[i].NET_UTIL_OUT +"%</b></div>";



            var marker = new L.Marker(location, {
              icon: blueDot
            });
            marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ sitename + ministry_agency + site_id + net_speed + net_health + net_utilin + net_utilout +"</div>", {maxWidth: '400'});
            vm.users.addLayer(marker);
          }
		
	};
	
	$scope.filter = function() {
		vm.showSites();
		vm.showStats();
	};
	
	$scope.reset = function() {
		$scope.filters = {
			'NET_HEALTH' : '0',
			'SITE_STATE' : '0',
			'MINISTRY' : '0',
			'AGENCY' : '0',
			'SITE_NAME' : ''
		};
		//vm.users.clearLayers();
		vm.showSites();
		vm.showStats();
	};
	
	vm.refresh = function() {
		console.log("Interval occurred");
		vm.showSites();
		vm.showStats();
	};
	
	$(document).ready(function() {
        $.ajaxSetup({cache:false});
        $('#map').css('height', ($(window).height() - 40));
        vm.showSites();
		vm.showStats();
		vm.showMinistries();
		vm.showAgencies();
		$interval(vm.refresh, vm.refreshFrequency);
      });

      $(window).resize(function () {
        $('#map').css('height', ($(window).height() - 40));
      }).resize();
	
}]);