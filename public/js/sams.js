var samsApp = angular.module('samsApp', ['ngRoute','samsAppControllers'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });

samsApp.config(['$routeProvider', function($routeProvider) {
    
    $routeProvider.

    when('/', {
        templateUrl: 'index.php',
        controller: 'IndexController'
    });

}]);
/*
samsApp.directive('myCurrentTime', ['$interval', 'dateFilter',
	function($interval, dateFilter) {
		return function(scope, element, attrs) {
			var format,  // date format
				stopTime; // so that we can cancel the time updates

			// used to update the UI
			function updateTime() {
				element.text(dateFilter(new Date(), format));
			}

			// watch the expression, and update the UI on change.
			scope.$watch(attrs.myCurrentTime, function(value) {
				format = value;
				updateTime();
			});

			stopTime = $interval(updateTime, 10000);

			// listen on DOM destroy (removal) event, and cancel the next UI update
			// to prevent updating time after the DOM element was removed.
			element.on('$destroy', function() {
				$interval.cancel(stopTime);
			});
        }		
	}
]);	*/
