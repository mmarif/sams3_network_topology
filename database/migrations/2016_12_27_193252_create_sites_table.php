<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('sites', function (Blueprint $table) {
			$table->string('SITE_ID');
            $table->string('SITE_NAME')->nullable();
            $table->string('MINISTRY')->nullable();
            $table->string('MINISTRY_AGENCY')->nullable();
            $table->string('AGENCY')->nullable();
            $table->string('NET_SPEED')->nullable();
            $table->string('BMT_URL')->nullable();
            $table->string('NET_UTIL_IN')->nullable();
            $table->string('NET_UTIL_OUT')->nullable();
            $table->string('NET_HEALTH')->nullable();
            $table->string('GPS_LAT')->nullable();
			$table->string('GPS_LONG')->nullable();
            $table->string('SITE_STATE')->nullable();			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sites');
    }
}
